var mongoose = require('mongoose');
var db = require('../db');
var ExtensionSchema = new mongoose.Schema({
    urls: {type: [], required: false}
}, {timestamps: true});

ExtensionSchema.plugin(db.autoIncrement.plugin, { model: 'Extension', field: 'extensionId' });


// mongoose.model('Extension', ExtensionSchema);
module.exports = mongoose.model('Extension', ExtensionSchema);
