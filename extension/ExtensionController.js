var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');


router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

var Extension = require('./Extension');

router.get('/create', function (req, res) {
    console.log("creating extension");

    Extension.create({urls: []}, function (err, extension) {
        if (err)
            return res.status(500).send("{'msg': 'Error'}");
        res.status(200).send(extension);
    })
});

router.get('/getrecomm', function(req, res){
    console.log('Getting recomms for extension id ' + req.query.extensionId);
    var extensionId = req.query.extensionId;
    extensionId = extensionId.toString();
    console.log(typeof extensionId);
    Extension.findOne({extensionId: extensionId}, function (err, extension) {
        console.log(extension);
        if (err)
            return res.status(500).send("{'msg': 'Error'}");

        if (!extension){
            Extension.create({extensionId: extensionId}, function (err, extension) {
                if (err)
                    return res.status(500).send("{'msg': 'Error'}");
                else
                    res.status(200).send(extension);
            })
        }
        else
            res.status(200).send(extension);
    })
});
// router.post('/history', function (req, res) {
//     var history = req.body.history;
//     var extensionId = req.body.extensionId;
//     console.log(extensionId);
//     // var userId = req.body.
//     for (var i = 0; i < history.length; i++) {
//         var historyItem = history[i];
//         // console.log("__" + historyItem);
//         var historyItemUrl = historyItem.split("::")[0];
//         var historyItemUrlParsed = url.parse(historyItemUrl);
//
//         var newUrl = new Url();
//         newUrl.domain = historyItemUrlParsed.host;
//         newUrl.path = historyItemUrlParsed.pathname;
//
//         Url.findOne({"domain": historyItemUrlParsed.host, "path": historyItemUrlParsed.pathname}, function (err, insertedUrl) {
//             if( err){
//                 console.log(err);
//             } else{
//                 console.log(insertedUrl);
//             }
//
//             if (!insertedUrl){
//                 urlController.createUrl(historyItemUrl);
//             }
//         })
//     }
//     return res.status(200).send({"msg": "success"});
// });

module.exports = router;