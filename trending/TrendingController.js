var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

var Trending = require('./trending');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get('/', function (req, res) {
    console.log("sending trending");
    Trending.find({}, function (err, trending) {
        if(err) {
            console.log("error");
            console.log(err);
            res.status(500).send("{'msg': 'error'}");
        } else {
            res.status(200).send(trending);
        }
    });
});

module.exports = router;