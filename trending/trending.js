var mongoose = require('mongoose');
var db = require('../db');
var TrendingSchema = new mongoose.Schema({
    url: {type: String, required: true}
}, {timestamps: true});

// TrendingSchema.plugin(db.autoIncrement.plugin, { model: 'Trending', field: 'trendingId' });


// mongoose.model('History', HistorySchema);
module.exports = mongoose.model('Trending', TrendingSchema);
