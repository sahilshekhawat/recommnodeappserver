const express = require('express');
const bodyParser= require('body-parser');
var db = require('./db');
var UserController = require('./user/UserController');
var ExtensionController = require('./extension/ExtensionController');
var UrlController = require('./url/UrlController');
var HistoryController = require('./history/HistoryController');
var TrendingController = require('./trending/TrendingController');

const app = express();

// var uri = "mongodb://sahilshekhawat:sahilshekhawt@recommdb-shard-00-00-hqspw.mongodb.net:27017,recommdb-shard-00-01-hqspw.mongodb.net:27017,recommdb-shard-00-02-hqspw.mongodb.net:27017/recommdb?ssl=true&replicaSet=recommdb-shard-0&authSource=admin";

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/users', UserController);
app.use('/extensions', ExtensionController);
app.use('/urls', UrlController);
app.use('/history', HistoryController);
app.use('/trending', TrendingController);

app.listen(3000, function () {
    console.log('listening on 3000')
});

module.exports = app;
