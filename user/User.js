var mongoose = require('mongoose');
var db = require('../db');
var UserSchema = new mongoose.Schema({
    username: String
});

UserSchema.plugin(db.autoIncrement.plugin, { model: 'User', field: 'userId' });


mongoose.model('User', UserSchema);
module.exports = mongoose.model('User');