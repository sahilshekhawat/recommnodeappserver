var mongoose = require('mongoose');
var db = require('../db');
var UrlSchema = new mongoose.Schema({
    imageUrl: {type: String, required: false},
    title: {type: String, required: false},
    desc: {type: String, required: false},
    domain: {type: String, required: true},
    path: {type: String, required: true}
}, {timestamps: true});

UrlSchema.plugin(db.autoIncrement.plugin, { model: 'Url', field: 'urlId' });

// mongoose.model('Url', UrlSchema);
module.exports = mongoose.model('Url', UrlSchema);
