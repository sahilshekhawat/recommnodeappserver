var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
const url = require('url');
var MetaInspector = require('node-metainspector');
var ImageResolver = require('image-resolver');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

var Url = require('./Url');

router.post('/create', function (req, res) {
    console.log("creating url");
    var result = createUrl(req.body.url);
    if(result.error)
        return res.status(500).send("{'msg': 'Error'}");
    else {
        return res.status(200).send(url);
    }
});

function createUrl(urlInput){
    var normalUrl = urlInput;
    var urlInput = url.parse(urlInput);
    console.log("creating url");
    Url.create({
        domain: urlInput.host,
        path: urlInput.pathname
    }, function (error, url) {
        if (error)
            console.log(error);
        console.log(url.id);
        if(!error)
            getImageAndTitle(normalUrl, url);

        var result = {};
        result.error = error;
        result.url = url;
        return result;
    });
}

function getImageAndTitle(urlInput, newUrl){
    var client = new MetaInspector(urlInput, { timeout: 5000 });
    client.on("fetch", function(){
        console.log(newUrl);
        console.log("444");
        Url.findOneAndUpdate(
            {'urlId': newUrl.urlId},
            {'title': client.title},
            {},
            function(err, doc){
                if (err) console.log(err);
                else console.log("title updated");
            });
    });

    client.on("error", function(err){
        console.log(err);
    });
    client.fetch(urlInput);

    // Getting main image from url;
    // var urlInput = url.parse(req.body.url);
    var resolver = new ImageResolver();
    resolver.register(new ImageResolver.FileExtension());
    resolver.register(new ImageResolver.MimeType());
    resolver.register(new ImageResolver.Opengraph());
    resolver.register(new ImageResolver.Webpage());
    resolver.resolve( urlInput, function( result ){
        if ( result ) {
            console.log( result.image );
            Url.findOneAndUpdate(
                {'urlId': newUrl.urlId},
                {'imageUrl': result.image},
                {},
                function(err, doc){
                    if (err) console.log(err);
                    else console.log("imageUrl updated");
                });
        } else {
            console.log( "No image found" );
        }
    });
}

router.get('/', function (req, res) {
    var query = Url.findOne({'urlId': req.query.id});
    query.exec(function (error, url) {
        if (error) res.status(500).send({"msg": "Error"});
        else res.status(200).send(url);
    })
});

module.exports = router;
module.exports.getImageAndTitle = getImageAndTitle;
module.exports.createUrl = createUrl;