var mongoose = require('mongoose');
var db = require('../db');
var HistorySchema = new mongoose.Schema({
    extensionId: {type: String, required: true},
    history: {type: Object, required: true}
}, {timestamps: true});

HistorySchema.plugin(db.autoIncrement.plugin, { model: 'Url', field: 'urlId' });


// mongoose.model('History', HistorySchema);
module.exports = mongoose.model('History', HistorySchema);
