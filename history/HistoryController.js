var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var urlController = require('../url/UrlController');
const url = require('url');

var History = require('./history');
var Url = require('../url/Url');

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.post('/', function (req, res) {
    var history = req.body.history;
    var extensionId = req.body.extensionId;
    console.log(extensionId);
    // console.log(req);
    // var userId = req.body.
    var formattedHistory = {};
    // for (var i = 0; i < history.length; i++) {
    //     var historyItem = history[i];
    //     // console.log("__" + historyItem);
    //     var historyItemUrl = historyItem.url;
    //     var historyItemLastVisitTime;
    //     var historyItemUrlParsed = url.parse(historyItemUrl);
    //     formattedHistory["url"] = historyItemUrl;
    //     formattedHistory["lastVisitTime"] = historyItem.lastVisitTime;
    //
    //     // var newUrl = new Url();
    //     // newUrl.domain = historyItemUrlParsed.host;
    //     // newUrl.path = historyItemUrlParsed.pathname;
    //     console.log(historyItemUrl);
    //     // TODO use ids instead of original url.
    //     // Url.findOne({"domain": historyItemUrlParsed.host, "path": historyItemUrlParsed.pathname}, function (err, insertedUrl) {
    //     //     if( err){
    //     //         console.log(err);
    //     //     } else{
    //     //         console.log(insertedUrl);
    //     //     }
    //     //
    //     //     if (!insertedUrl){
    //     //         urlController.createUrl(historyItemUrl);
    //     //     }
    //     // })
    // }

    History.create({
        "extensionId": extensionId,
        "history": history
    }, function (err, history) {
        if(err) {
            console.log("error");
            console.log(err);
        }
        else {
            console.log(history);
            console.log("History Inserted");
        }
    });

    return res.status(200).send({"msg": "success"});
});

module.exports = router;